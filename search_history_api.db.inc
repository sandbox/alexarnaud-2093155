<?php
/**
 * @file
 * db call functions for search history api module.
 */

/**
 * Save a search.
 *
 * @param int $uid
 *   User id that did the search.
 *
 * @param string $type
 *   Search type. A string that tells where the search come from.
 *
 * @param array $data
 *   An array containing search informatoin (path, url, options).
 */
function search_history_api_insert($uid, $type, $data) {
  if (user_access('save search history')) {
    $result = db_insert('search_history')
      ->fields(array(
        'uid' => $uid,
        'search_type' => $type,
        'data' => serialize($data),
        'created' => REQUEST_TIME,
    ))
    ->execute();
  }
}

/**
 * Update a search.
 *
 * @param int $sid
 *   Saved search identifier.
 *
 * @param array $data
 *   An array containing search informatoin (path, url, options). 
 *
 * @return bool
 *   True if update worked.
 */
function search_history_api_update($sid, $data) {
  $num_updated = db_update('search_history')
    ->fields(array('data' => serialize($data)))
    ->condition('sid', $sid)
    ->execute();

  if ($num_updated == 1) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Get saved searches for a given user or all.
 *
 * @param int $uid
 *   User id to get seraches.
 *
 * @return array
 *   All searches keyed by sid.
 */
function search_history_api_load($uid = NULL) {
  $query = db_select('search_history', 's')
    ->fields('s')
    ->orderBy('created', 'DESC');

  if (isset($uid)) {
    $query->condition('uid', $uid);
  }

  $result = $query->execute();
  $saved = array();
  while ($record = $result->fetchAssoc()) {
    $saved[$record['sid']] = $record;
  }
  return $saved;
}

/**
 * Delete a search.
 *
 * @param int $sid
 *   Unique search identifier to delete.
 */
function search_history_api_delete($sid) {
  $num_deleted = db_delete('search_history')
    ->condition('sid', $sid)
    ->execute();
}
