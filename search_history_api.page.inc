<?php
/**
 * @file
 * Forms and pages created by search history api module.
 */

/**
 * Form constructor for the search history page.
 *
 * Allows user to view their search history.
 *
 * @see opac_admin_form_validate()
 * @ingroup forms
 */
function search_history_api_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'search_history_api', 'search_history_api.db');
  global $user;
  $searches = search_history_api_load($user->uid);
  $form = array();

  if (isset($form_state['values']['searches'])) {
    $to_delete = array_filter($form_state['values']['searches']);
    $form['searches'] = array(
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
      '#tree' => TRUE,
    );
    foreach ($to_delete as $sid) {
      $data = unserialize($searches[$sid]['data']);
      $description = $data['description'];
      $string = "<b>$description</b> from " . format_date($searches[$sid]['created'], 'long');
      $form['searches'][$sid] = array(
        '#type' => 'hidden',
        '#value' => $sid,
        '#prefix' => '<li>',
        '#suffix' => $string . "</li>\n",
      );
    }

    $form['#submit'][] = 'searches_multiple_delete_confirm_submit';

    $confirm_question = format_plural(count($to_delete),
                                    'Are you sure you want to delete this search?',
                                    'Are you sure you want to delete these searches?');

    return confirm_form($form,
                      $confirm_question,
                      'user/' . $user->uid . '/search-history', t('This action cannot be undone.'),
                      t('Delete'), t('Cancel'));
  }

  elseif ($user->uid) {
    $header = array(
      'created' => t('Date'),
      'search' => t('Search'),
      'type' => t('On'),
      'actions' => t('Actions'),
    );

    $options = array();
    foreach ($searches as $id => $search) {
      $data = unserialize($search['data']);
      $path_options = isset($data['path_options']) ? $data['path_options'] : array();

      // Add a flag (nosave) in url to know we don't
      // have to save this search again if
      // the user click on the search link.
      $path_options['query']['nosave'] = 1;

      $options[$id] = array(
        'created' => format_date($search['created'], 'long'),
        'search' => l($data['description'], urldecode($data['path']), $path_options),
        'type' => $search['search_type'],
        '#attributes' => array('class' => array('search-row')),
      );

      // Add a flag in url to remember
      // the user cliked on modify link.
      // So, the search will be updated
      // after it will be launched again.
      $path_options['query']['sid'] = $id;

      // Add modify link.
      $options[$id]['actions'] = l(t('Modify'), urldecode($data['path']), $path_options);
    }

    $form['searches'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t("You didn't make search yet."),
    );

    $form['pager'] = array('#value' => theme('pager'));
    $form['submit'] = array('#type' => 'submit', '#value' => t('Delete'));
    return $form;
  }
}

/**
 * Form constructor for deleting searches.
 *
 * @ingroup forms
 */
function searches_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach (array_keys($form_state['values']['searches']) as $sid) {
      search_history_api_delete($sid);
    }
    $count = count($form_state['values']['searches']);
    drupal_set_message(format_plural($count, 'Deleted 1 search.', 'Deleted @count searches.'));
  }
}

/**
 * Form validation handler for search_history_api_form().
 */
function search_history_api_form_validate($form, &$form_state) {
  // Error if there are no search to delete.
  if (!count(array_filter($form_state['values']['searches']))) {
    form_set_error('', t('No search selected.'));
  }
}

/**
 * Form submit handler for search_history_api_form().
 */
function search_history_api_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}
